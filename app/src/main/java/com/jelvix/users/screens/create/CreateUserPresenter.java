/*
 * Copyright (c) Anfisa Tkachova anfiska.uvarova@gmail.com
 */

package com.jelvix.users.screens.create;

import com.jelvix.users.domain.UserInteractor;
import com.jelvix.users.network.ApiProvider;
import com.jelvix.users.network.entities.CreateUserRequest;
import com.jelvix.users.network.entities.CreateUserResponse;
import com.jelvix.users.utils.CommonUtils;
import com.jelvix.users.utils.Constants;
import com.jelvix.users.utils.RequestErrorHandler;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Anfisa on 19.08.2017.
 */

class CreateUserPresenter implements CreateUserContract.Presenter {

    private final CreateUserContract.View view;
    private final UserInteractor userInteractorr;
    private Disposable createUserSubscription;

    CreateUserPresenter(CreateUserContract.View view) {
        this.view = view;
        this.userInteractorr = new UserInteractor(ApiProvider.getApi());
    }

    @Override
    public void onRegisterButtonClicked() {
        if (isValidUserData()) {
            view.showProgress();
            CreateUserRequest createUserRequest = new CreateUserRequest();
            createUserRequest.setName(view.getUserFirstName() + " " + view.getUserLastName());
            createUserRequest.setJob(view.getUserJob());
            Observable<CreateUserResponse> observable = userInteractorr.createUser(createUserRequest);
            createUserSubscription = observable
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnError(throwable -> {
                        view.hideProgress();
                        view.showRequestError(RequestErrorHandler.getRequesrErrorMessage(throwable));
                    })
                    .subscribe(s -> {
                        view.hideProgress();
                        view.navigateToMainUsersList();
                    });
        }
    }

    @Override
    public void onDestroy() {
        if (createUserSubscription != null && !createUserSubscription.isDisposed()) {
            createUserSubscription.dispose();
        }
    }

    private boolean isValidUserData() {
        boolean valid = true;
        if (view.getUserFirstName().length() == 0 || !CommonUtils.isValidName(view.getUserFirstName())) {
            valid = false;
            view.setError(Constants.FIRST_NAME);
        }
        if (view.getUserLastName().length() == 0 || !CommonUtils.isValidName(view.getUserLastName())) {
            valid = false;
            view.setError(Constants.LAST_NAME);
        }
        if (view.getUserJob().length() == 0) {
            valid = false;
            view.setError(Constants.JOB);
        }
        return valid;
    }

}
