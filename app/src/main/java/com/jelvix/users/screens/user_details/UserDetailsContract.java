/*
 * Copyright (c) Anfisa Tkachova anfiska.uvarova@gmail.com
 */

package com.jelvix.users.screens.user_details;

import com.jelvix.users.base.BaseView;

/**
 * Created by Anfisa on 19.08.2017.
 */

interface UserDetailsContract {

    interface View extends BaseView {
        void uploadImage(String image);

        void setUserName(String userName);
    }

    interface Presenter {
        void getUserData(int id);

        void onDestroy();
    }
}
