/*
 * Copyright (c) Anfisa Tkachova anfiska.uvarova@gmail.com
 */

package com.jelvix.users.screens.create;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.jelvix.users.R;
import com.jelvix.users.base.BaseFragment;
import com.jelvix.users.screens.users_list.UsersListFragment;
import com.jelvix.users.utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Anfisa on 19.08.2017.
 */

public class CreateUserFragment extends BaseFragment implements CreateUserContract.View {

    private CreateUserContract.Presenter presenter;

    @BindView(R.id.user_first_name_et)
    EditText firstNameEt;
    @BindView(R.id.user_last_name_et)
    EditText lastNameEt;
    @BindView(R.id.user_job_et)
    EditText jobEt;
    @BindView(R.id.register_btn)
    Button registerBtn;

    public static CreateUserFragment newInstance() {
        CreateUserFragment fragment = new CreateUserFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new CreateUserPresenter(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_register, container, false);
        ButterKnife.bind(this, view);
        setToolbarVisibility(false);
        registerBtn.setOnClickListener(view1 -> presenter.onRegisterButtonClicked());
        return view;
    }

    @Override
    public String getUserFirstName() {
        return firstNameEt.getText().toString();
    }

    @Override
    public String getUserLastName() {
        return lastNameEt.getText().toString();
    }

    @Override
    public String getUserJob() {
        return jobEt.getText().toString();
    }

    @Override
    public void setError(String key) {
        switch (key) {
            case Constants.FIRST_NAME:
                firstNameEt.setError(getActivity().getString(R.string.invalid_first_name));
                break;
            case Constants.LAST_NAME:
                lastNameEt.setError(getActivity().getString(R.string.invalid_last_name));
                break;
            case Constants.JOB:
                jobEt.setError(getActivity().getString(R.string.invalid_job));
                break;
        }
    }

    @Override
    public void navigateToMainUsersList() {
        getFragmentStack().replace(UsersListFragment.newInstance());
    }

    @Override
    public void showProgress() {
        setProgressVisibility(true);
    }

    @Override
    public void hideProgress() {
        setProgressVisibility(false);
    }

    @Override
    public void showRequestError(String error) {
        showToast(error);
    }
}

