/*
 * Copyright (c) Anfisa Tkachova anfiska.uvarova@gmail.com
 */

package com.jelvix.users.screens.users_list;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jelvix.users.R;
import com.jelvix.users.base.BaseFragment;
import com.jelvix.users.model.entities.User;
import com.jelvix.users.screens.user_details.UserDetailsFragment;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Anfisa on 17.08.2017.
 */

public class UsersListFragment extends BaseFragment implements UsersListContract.View, UsersListAdapter.OnItemClickListener {

    private UsersListAdapter usersListAdapter;
    private UsersListContract.Presenter presenter;

    @BindView(R.id.recycler)
    RecyclerView usersRecyclerView;

    public static UsersListFragment newInstance() {
        UsersListFragment fragment = new UsersListFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        usersListAdapter = new UsersListAdapter(getActivity());
        usersListAdapter.setListener(this);
        presenter = new UsersListPresenter(this);
        presenter.getUsersData();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_users_list, container, false);
        ButterKnife.bind(this, view);
        setToolbarVisibility(true);
        setToolbarTitle(getActivity().getString(R.string.users_list));
        usersRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        usersRecyclerView.setAdapter(usersListAdapter);
        return view;
    }

    @Override
    public void updateData(List<User> usersList) {
        usersListAdapter.updateData(usersList);
    }

    @Override
    public void onItemClicked(int id) {
        getFragmentStack().push(UserDetailsFragment.newInstance(id));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    @Override
    public void showProgress() {
        setProgressVisibility(true);
    }

    @Override
    public void hideProgress() {
        setProgressVisibility(false);
    }

    @Override
    public void showRequestError(String errorMessage) {
        showToast(errorMessage);
    }
}
