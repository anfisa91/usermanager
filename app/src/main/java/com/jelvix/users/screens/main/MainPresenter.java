/*
 * Copyright (c) Anfisa Tkachova anfiska.uvarova@gmail.com
 */

package com.jelvix.users.screens.main;

import com.jelvix.users.model.DatabaseHelper;
import com.jelvix.users.utils.UserPreferences;

/**
 * Created by Anfisa on 19.08.2017.
 */

class MainPresenter implements MainContract.Presenter {
    private final MainContract.View view;

    MainPresenter(MainContract.View view) {
        this.view = view;
    }

    @Override
    public void onLogoutClicked() {
        UserPreferences.getInstance().clearUserRegisterInfo();
        DatabaseHelper.get().clearDb();
        view.showLoginScreen();
    }

    @Override
    public void onAutoLogin() {
        if (UserPreferences.getInstance().getUserToken().length() > 0) {
            view.showUsersScreen();
        } else {
            view.showLoginScreen();
        }
    }
}
