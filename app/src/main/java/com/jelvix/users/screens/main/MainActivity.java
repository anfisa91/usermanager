/*
 * Copyright (c) Anfisa Tkachova anfiska.uvarova@gmail.com
 */

package com.jelvix.users.screens.main;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.jelvix.users.R;
import com.jelvix.users.base.BaseActivity;
import com.jelvix.users.utils.FragmentsStack;
import com.jelvix.users.screens.login_register.LoginFragment;
import com.jelvix.users.screens.users_list.UsersListFragment;
import com.jelvix.users.utils.CommonUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity implements MainContract.View {

    private boolean mToolBarNavigationListener = false;
    private FragmentsStack fragmentsStack;
    private MainContract.Presenter presenter;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.fragment_container)
    FrameLayout fragmentContainer;
    @BindView(R.id.toolbar_title_tv)
    TextView titleTextView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.toolbar_logout_image)
    ImageView toolbarLogoutImage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        presenter = new MainPresenter(this);
        fragmentsStack = new FragmentsStack(getFragmentManager(), R.id.fragment_container);
        if (savedInstanceState == null) {
            presenter.onAutoLogin();
        }
        toolbarLogoutImage.setOnClickListener(view -> {
            showLogoutDialog();
        });
        setToolbarTitle(getString(R.string.app_name));
        fragmentsStack.getFragmentManager().addOnBackStackChangedListener(this::shouldDisplayHomeUp);
        shouldDisplayHomeUp();
    }

    //Check and show navigate back button depending on fragments stack count
    public void shouldDisplayHomeUp() {
        boolean isHaveFragmentsStack = getFragmentManager().getBackStackEntryCount() > 0;
        if (isHaveFragmentsStack) {
            toolbar.setNavigationIcon(android.support.v7.appcompat.R.drawable.abc_ic_ab_back_material);
            if (!mToolBarNavigationListener) {
                toolbar.setNavigationOnClickListener(v -> {
                    CommonUtils.hideKeyboard(MainActivity.this);
                    onBackPressed();
                });
                mToolBarNavigationListener = true;
            }
        } else {
            toolbar.setNavigationIcon(null);
            toolbar.setNavigationOnClickListener(null);
            mToolBarNavigationListener = false;
        }
    }

    @Override
    public FragmentsStack getFragmentsStack() {
        return fragmentsStack;
    }

    @Override
    public void setToolbarVisibility(boolean isVisible) {
        if (isVisible) {
            toolbar.setVisibility(View.VISIBLE);
        } else {
            toolbar.setVisibility(View.GONE);
        }
    }

    @Override
    public void setToolbarTitle(String title) {
        titleTextView.setText(title);
    }

    @Override
    public void setProgressVisibility(boolean isVisible) {
        if (isVisible) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }

    public void showLogoutDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.CustomDialogTheme);
        builder.setTitle(getString(R.string.logout))
                .setMessage(getString(R.string.sure_want_logout))
                .setCancelable(true)
                .setPositiveButton(getResources().getString(R.string.ok),
                        (dialog, id) -> {
                            presenter.onLogoutClicked();
                            dialog.cancel();
                        })
                .setNegativeButton(getResources().getString(R.string.cancel),
                        (dialog, id) -> dialog.cancel());
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void showLoginScreen() {
        fragmentsStack.replace(LoginFragment.newInstance());
    }

    @Override
    public void showUsersScreen() {
        fragmentsStack.replace(UsersListFragment.newInstance());
    }
}
