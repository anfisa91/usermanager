/*
 * Copyright (c) Anfisa Tkachova anfiska.uvarova@gmail.com
 */

package com.jelvix.users.screens.login_register;

import com.jelvix.users.base.BaseView;

/**
 * Created by Anfisa on 18.08.2017.
 */

interface LoginRegisterContract {
    interface View extends BaseView {

        String getEmail();

        String getPassword();

        String getConfirmPassword();

        void setError(String key);

        void navigateAfterRegister();

        void navigateAfterLogin();

        void setViewLoginType();

        void setViewRegisterType();
    }


    interface Presenter {
        void onLoginRegisterClicked();

        void changeAuthType();

        void onDestroy();
    }
}
