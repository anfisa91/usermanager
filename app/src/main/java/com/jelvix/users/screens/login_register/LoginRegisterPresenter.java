/*
 * Copyright (c) Anfisa Tkachova anfiska.uvarova@gmail.com
 */

package com.jelvix.users.screens.login_register;

import com.jelvix.users.domain.LoginInteractor;
import com.jelvix.users.network.ApiProvider;
import com.jelvix.users.network.entities.LoginRegisterRequest;
import com.jelvix.users.utils.CommonUtils;
import com.jelvix.users.utils.Constants;
import com.jelvix.users.utils.RequestErrorHandler;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


/**
 * Created by Anfisa on 18.08.2017.
 */

class LoginRegisterPresenter implements LoginRegisterContract.Presenter {

    private final int MIN_PASS_LENGTH = 2;

    private final LoginRegisterContract.View view;
    private final LoginInteractor loginInteractor;
    private Disposable loginUserSubscription;
    private int currentAuthType;

    LoginRegisterPresenter(LoginRegisterContract.View view) {
        this.view = view;
        loginInteractor = new LoginInteractor(ApiProvider.getApi());
        currentAuthType = Constants.AUTH_TYPE_LOGIN;
    }

    @Override
    public void onLoginRegisterClicked() {
        if (isValidLoginData()) {
            view.showProgress();
            if (currentAuthType == Constants.AUTH_TYPE_LOGIN) {
                loginRequest();
            } else {
                registerRequest();
            }
        }
    }

    private void loginRequest() {
        LoginRegisterRequest loginRegisterRequest = new LoginRegisterRequest();
        loginRegisterRequest.setEmail(view.getEmail());
        loginRegisterRequest.setPassword(view.getPassword());
        Observable<String> observable = loginInteractor.loginUser(loginRegisterRequest);
        loginUserSubscription = observable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError(throwable -> {
                    view.hideProgress();
                    view.showRequestError(RequestErrorHandler.getRequesrErrorMessage(throwable));
                })
                .subscribe(s -> {
                    view.hideProgress();
                    view.navigateAfterLogin();
                });
    }

    private void registerRequest() {
        LoginRegisterRequest loginRegisterRequest = new LoginRegisterRequest();
        loginRegisterRequest.setEmail(view.getEmail());
        loginRegisterRequest.setPassword(view.getPassword());
        Observable<String> observable = loginInteractor.registerUser(loginRegisterRequest);
        loginUserSubscription = observable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError(throwable -> {
                    view.hideProgress();
                    view.showRequestError(RequestErrorHandler.getRequesrErrorMessage(throwable));
                })
                .subscribe(s -> {
                    view.hideProgress();
                    view.navigateAfterRegister();
                });
    }


    private boolean isValidLoginData() {
        boolean valid = true;
        if (view.getEmail().length() == 0 || !CommonUtils.isValidEmail(view.getEmail())) {
            valid = false;
            view.setError(Constants.EMAIL);
        }
        if (view.getPassword().length() < MIN_PASS_LENGTH) {
            valid = false;
            view.setError(Constants.PASSWORD);
        }
        if (currentAuthType == Constants.AUTH_TYPE_REGISTER && !view.getPassword().equals(view.getConfirmPassword())) {
            valid = false;
            view.setError(Constants.CONFIRM_PASSWORD);
        }
        return valid;
    }

    @Override
    public void changeAuthType() {
        if (currentAuthType == Constants.AUTH_TYPE_LOGIN) {
            currentAuthType = Constants.AUTH_TYPE_REGISTER;
            view.setViewRegisterType();
        } else {
            currentAuthType = Constants.AUTH_TYPE_LOGIN;
            view.setViewLoginType();
        }
    }

    @Override
    public void onDestroy() {
        if (loginUserSubscription != null && !loginUserSubscription.isDisposed()) {
            loginUserSubscription.dispose();
        }
    }
}
