package com.jelvix.users.screens.splash;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.jelvix.users.screens.main.MainActivity;

/**
 * Created by Anfisa on 17.08.2017.
 */

public class SplashActivity extends AppCompatActivity implements SplashActivityContract.View {

    private SplashActivityContract.Presenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new SplashPresenter(this);
        presenter.delayForNavigate();
    }

    @Override
    public void navigateToMainScreen() {
        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
