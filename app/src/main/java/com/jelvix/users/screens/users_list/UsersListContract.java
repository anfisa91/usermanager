/*
 * Copyright (c) Anfisa Tkachova anfiska.uvarova@gmail.com
 */

package com.jelvix.users.screens.users_list;

import com.jelvix.users.base.BaseView;
import com.jelvix.users.model.entities.User;

import java.util.List;

/**
 * Created by Anfisa on 17.08.2017.
 */

public interface UsersListContract {
    interface View extends BaseView {
        void updateData(List<User> userResponseList);
    }

    interface Presenter {
        void getUsersData();

        void onDestroy();
    }
}
