/*
 * Copyright (c) Anfisa Tkachova anfiska.uvarova@gmail.com
 */

package com.jelvix.users.screens.create;

import com.jelvix.users.base.BaseView;

/**
 * Created by Anfisa on 19.08.2017.
 */

interface CreateUserContract {
    interface View extends BaseView {
        String getUserFirstName();

        String getUserLastName();

        String getUserJob();

        void setError(String key);

        void navigateToMainUsersList();
    }

    interface Presenter {
        void onRegisterButtonClicked();

        void onDestroy();
    }
}
