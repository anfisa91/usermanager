/*
 * Copyright (c) Anfisa Tkachova anfiska.uvarova@gmail.com
 */

package com.jelvix.users.screens.user_details;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jelvix.users.R;
import com.jelvix.users.base.BaseFragment;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Anfisa on 19.08.2017.
 */

public class UserDetailsFragment extends BaseFragment implements UserDetailsContract.View {

    private static final String USER_ID = "user_id";
    private UserDetailsContract.Presenter presenter;
    private int userId;

    @BindView(R.id.user_details_imageView)
    ImageView userImage;
    @BindView(R.id.user_details_name)
    TextView userNameTv;

    public static UserDetailsFragment newInstance(int id) {
        UserDetailsFragment fragment = new UserDetailsFragment();
        Bundle args = new Bundle();
        args.putSerializable(USER_ID, id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new UserDetailsPresenter(this);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            userId = (int) bundle.get(USER_ID);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_details, container, false);
        ButterKnife.bind(this, view);
        setToolbarVisibility(true);
        setToolbarTitle(getActivity().getString(R.string.user_detail));
        presenter.getUserData(userId);
        return view;
    }

    @Override
    public void uploadImage(String image) {
        Picasso.with(getActivity()).load(image).error(R.mipmap.ic_launcher).into(userImage);
    }

    @Override
    public void setUserName(String userName) {
        userNameTv.setText(userName);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    @Override
    public void showProgress() {
        setProgressVisibility(true);
    }

    @Override
    public void hideProgress() {
        setProgressVisibility(false);
    }

    @Override
    public void showRequestError(String errorMessage) {
        showToast(errorMessage);
    }
}
