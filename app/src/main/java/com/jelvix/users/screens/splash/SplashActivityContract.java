package com.jelvix.users.screens.splash;

/**
 * Created by Anfisa on 17.08.2017.
 */

interface SplashActivityContract {
    interface View {
        void navigateToMainScreen();
    }

    interface Presenter {
        void delayForNavigate();
    }

}
