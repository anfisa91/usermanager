/*
 * Copyright (c) Anfisa Tkachova anfiska.uvarova@gmail.com
 */

package com.jelvix.users.screens.user_details;

import com.jelvix.users.domain.UserInteractor;
import com.jelvix.users.model.entities.User;
import com.jelvix.users.network.ApiProvider;
import com.jelvix.users.utils.RequestErrorHandler;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Anfisa on 19.08.2017.
 */

class UserDetailsPresenter implements UserDetailsContract.Presenter {

    private final UserInteractor userInteractor;
    private final UserDetailsContract.View view;
    private Disposable userDetailsSubscription;

    UserDetailsPresenter(UserDetailsContract.View view) {
        this.view = view;
        userInteractor = new UserInteractor(ApiProvider.getApi());
    }

    @Override
    public void getUserData(int id) {
        view.showProgress();
        Observable<User> observable = userInteractor.getUser(id);
        userDetailsSubscription = observable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError(throwable -> {
                    view.hideProgress();
                    view.showRequestError(RequestErrorHandler.getRequesrErrorMessage(throwable));
                })
                .subscribe(usersListResponse -> {
                    view.hideProgress();
                    setDetails(usersListResponse);
                });
    }

    @Override
    public void onDestroy() {
        if (userDetailsSubscription != null && !userDetailsSubscription.isDisposed()) {
            userDetailsSubscription.dispose();
        }
    }

    private void setDetails(User user) {
        String userName;
        String firstName = "";
        String lastName = "";
        String userImage = "";
        if (user.getFirstName() != null && user.getFirstName().length() > 0) {
            firstName = user.getFirstName();
        }
        if (user.getLastName() != null && user.getLastName().length() > 0) {
            lastName = user.getLastName();
        }
        userName = firstName + " " + lastName;
        if (user.getImage() != null && user.getImage().length() > 0 && user.getImage().contains("http")) {
            userImage = user.getImage();
        }
        view.setUserName(userName);
        view.uploadImage(userImage);
    }

}
