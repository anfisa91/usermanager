/*
 * Copyright (c) Anfisa Tkachova anfiska.uvarova@gmail.com
 */

package com.jelvix.users.screens.login_register;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.jelvix.users.R;
import com.jelvix.users.base.BaseFragment;
import com.jelvix.users.screens.create.CreateUserFragment;
import com.jelvix.users.screens.users_list.UsersListFragment;
import com.jelvix.users.utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Anfisa on 17.08.2017.
 */

public class LoginFragment extends BaseFragment implements LoginRegisterContract.View {

    private LoginRegisterContract.Presenter presenter;

    @BindView(R.id.login_email_et)
    EditText emailLoginEt;
    @BindView(R.id.login_password_et)
    EditText passwordLoginEt;
    @BindView(R.id.login_btn)
    Button loginBtn;
    @BindView(R.id.login_confirm_password_et)
    EditText confirmPasswordEt;
    @BindView(R.id.auth_type_tv)
    TextView loginTypeTv;

    public static LoginFragment newInstance() {
        LoginFragment fragment = new LoginFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new LoginRegisterPresenter(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login_register, container, false);
        ButterKnife.bind(this, view);
        setToolbarVisibility(false);
        loginBtn.setOnClickListener(view1 -> presenter.onLoginRegisterClicked());
        loginTypeTv.setOnClickListener(view1 -> presenter.changeAuthType());
        return view;
    }

    @Override
    public String getEmail() {
        return emailLoginEt.getText().toString();
    }

    @Override
    public String getPassword() {
        return passwordLoginEt.getText().toString();
    }

    @Override
    public String getConfirmPassword() {
        return confirmPasswordEt.getText().toString();
    }

    @Override
    public void setError(String key) {
        switch (key) {
            case Constants.EMAIL:
                emailLoginEt.setError(getActivity().getString(R.string.invalid_email));
                break;
            case Constants.PASSWORD:
                passwordLoginEt.setError(getActivity().getString(R.string.invalid_password));
                break;
            case Constants.CONFIRM_PASSWORD:
                confirmPasswordEt.setError(getActivity().getString(R.string.invalid_password));
                break;
        }
    }

    @Override
    public void setViewLoginType() {
        loginBtn.setText(getString(R.string.login));
        loginTypeTv.setText(getString(R.string.register));
        confirmPasswordEt.setVisibility(View.GONE);
    }

    @Override
    public void setViewRegisterType() {
        loginBtn.setText(getString(R.string.register));
        loginTypeTv.setText(getString(R.string.login));
        confirmPasswordEt.setVisibility(View.VISIBLE);
    }

    @Override
    public void showProgress() {
        setProgressVisibility(true);
    }

    @Override
    public void hideProgress() {
        setProgressVisibility(false);
    }

    @Override
    public void showRequestError(String errorMessage) {
        showToast(errorMessage);
    }

    @Override
    public void navigateAfterRegister() {
        getFragmentStack().replace(CreateUserFragment.newInstance());
    }

    @Override
    public void navigateAfterLogin() {
        getFragmentStack().replace(UsersListFragment.newInstance());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

}
