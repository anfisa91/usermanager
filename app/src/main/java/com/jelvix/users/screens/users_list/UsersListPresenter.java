/*
 * Copyright (c) Anfisa Tkachova anfiska.uvarova@gmail.com
 */

package com.jelvix.users.screens.users_list;

import com.jelvix.users.domain.UserInteractor;
import com.jelvix.users.model.entities.User;
import com.jelvix.users.network.ApiProvider;
import com.jelvix.users.utils.RequestErrorHandler;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Anfisa on 17.08.2017.
 */

public class UsersListPresenter implements UsersListContract.Presenter {

    private final UserInteractor usersInteractor;
    private final UsersListContract.View view;
    private Disposable userListSubscription;

    public UsersListPresenter(UsersListContract.View view) {
        this.view = view;
        usersInteractor = new UserInteractor(ApiProvider.getApi());
    }

    @Override
    public void getUsersData() {
        view.showProgress();
        String page = "2";
        Observable<List<User>> observable = usersInteractor.getUsersList(page);
        userListSubscription = observable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError(throwable -> {
                    view.hideProgress();
                    view.showRequestError(RequestErrorHandler.getRequesrErrorMessage(throwable));
                })
                .subscribe(usersListResponse -> {
                    view.hideProgress();
                    updateData(usersListResponse);
                });
    }

    @Override
    public void onDestroy() {
        if (userListSubscription != null && !userListSubscription.isDisposed()) {
            userListSubscription.dispose();
        }
    }

    private void updateData(List<User> userResponseList) {
        view.updateData(userResponseList);
    }

}
