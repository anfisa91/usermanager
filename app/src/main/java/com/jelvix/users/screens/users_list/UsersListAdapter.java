/*
 * Copyright (c) Anfisa Tkachova anfiska.uvarova@gmail.com
 */

package com.jelvix.users.screens.users_list;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jelvix.users.R;
import com.jelvix.users.model.entities.User;
import com.jelvix.users.utils.CommonUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Anfisa on 19.08.2017.
 */

public class UsersListAdapter extends RecyclerView.Adapter<UsersListAdapter.ViewHolder> {

    private List<User> usersList = new ArrayList<>();
    private UsersListAdapter.OnItemClickListener listener;
    private Context context;

    UsersListAdapter(Context context) {
        this.context = context;
    }

    void updateData(List<User> usersList) {
        this.usersList = usersList;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_users_list, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        final User user = usersList.get(position);
        viewHolder.cardView.setOnClickListener(view -> onItemClicked(usersList.get(viewHolder.getAdapterPosition()).getId()));
        String userName = user.getFirstName() + " " + user.getLastName();
        viewHolder.userName.setText(userName);
        Picasso.with(context).load(user.getImage()).error(R.mipmap.ic_launcher).into(viewHolder.userImage);
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.card_view)
        CardView cardView;
        @BindView(R.id.item_user_name)
        TextView userName;
        @BindView(R.id.user_image)
        ImageView userImage;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    void setListener(UsersListAdapter.OnItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public int getItemCount() {
        return CommonUtils.isListEmpty(usersList) ? 0 : usersList.size();
    }

    private void onItemClicked(int id) {
        if (listener != null) {
            listener.onItemClicked(id);
        }
    }

    //Interface to notify fragment about item click events.
    interface OnItemClickListener {
        void onItemClicked(int id);
    }
}
