package com.jelvix.users.screens.splash;

import java.util.concurrent.TimeUnit;

import io.reactivex.Completable;

/**
 * Created by Anfisa on 17.08.2017.
 */

class SplashPresenter implements SplashActivityContract.Presenter {

    private final int DELAY_TIME = 3;
    private final SplashActivityContract.View splashActivity;

    SplashPresenter(SplashActivity splashActivity) {
        this.splashActivity = splashActivity;
    }

    @Override
    public void delayForNavigate() {
        Completable.complete()
                .delay(DELAY_TIME, TimeUnit.SECONDS)
                .doOnComplete(splashActivity::navigateToMainScreen)
                .subscribe();
    }
}
