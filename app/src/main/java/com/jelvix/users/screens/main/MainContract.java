/*
 * Copyright (c) Anfisa Tkachova anfiska.uvarova@gmail.com
 */

package com.jelvix.users.screens.main;

/**
 * Created by Anfisa on 19.08.2017.
 */

interface MainContract {
    interface View {
        void showLoginScreen();

        void showUsersScreen();
    }

    interface Presenter {
        void onLogoutClicked();

        void onAutoLogin();
    }
}