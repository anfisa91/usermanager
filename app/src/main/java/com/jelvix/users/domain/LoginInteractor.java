/*
 * Copyright (c) Anfisa Tkachova anfiska.uvarova@gmail.com
 */

package com.jelvix.users.domain;

import com.jelvix.users.network.Api;
import com.jelvix.users.network.entities.LoginRegisterRequest;
import com.jelvix.users.utils.UserPreferences;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Anfisa on 18.08.2017.
 */

public class LoginInteractor {

    private Api api;

    public LoginInteractor(Api api) {
        this.api = api;
    }

    public Observable<String> loginUser(LoginRegisterRequest loginRegisterRequest) {
        return api.loginUser(loginRegisterRequest)
                .observeOn(Schedulers.computation())
                .map(loginRegisterResponse -> {
                    UserPreferences.getInstance().setUserToken(loginRegisterResponse.getToken());
                    return loginRegisterResponse.getToken();
                });
    }

    public Observable<String> registerUser(LoginRegisterRequest loginRegisterRequest) {
        return api.registerUser(loginRegisterRequest)
                .observeOn(Schedulers.computation())
                .map(loginRegisterResponse -> {
                    UserPreferences.getInstance().setUserToken(loginRegisterResponse.getToken());
                    return loginRegisterResponse.getToken();
                });
    }
}
