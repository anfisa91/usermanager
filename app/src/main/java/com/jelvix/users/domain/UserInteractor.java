/*
 * Copyright (c) Anfisa Tkachova anfiska.uvarova@gmail.com
 */

package com.jelvix.users.domain;

import com.jelvix.users.model.DatabaseHelper;
import com.jelvix.users.model.entities.User;
import com.jelvix.users.network.Api;
import com.jelvix.users.network.entities.CreateUserRequest;
import com.jelvix.users.network.entities.CreateUserResponse;
import com.jelvix.users.utils.UserPreferences;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Anfisa on 19.08.2017.
 */

public class UserInteractor {

    private Api api;

    public UserInteractor(Api api) {
        this.api = api;
    }

    public Observable<CreateUserResponse> createUser(CreateUserRequest createUserRequest) {
        return api.createUser(createUserRequest)
                .observeOn(Schedulers.computation())
                .map(createUserResponse -> {
                    UserPreferences.getInstance().setCurrentUserId(createUserResponse.getId());
                    return createUserResponse;
                });
    }

    public Observable<List<User>> getUsersList(String page) {
        return api.getUsersList(page)
                .observeOn(Schedulers.computation())
                .map(usersListResponses -> {
                    DatabaseHelper.get().getUserDao().saveAll(usersListResponses.getUser());
                    return DatabaseHelper.get().getUserDao().getAll();
                });
    }

    public Observable<User> getUser(int id) {
        return api.getCurrentUser(id)
                .observeOn(Schedulers.computation())
                .map(userResponse -> {
                    DatabaseHelper.get().getUserDao().save(userResponse.getUserResponse());
                    return DatabaseHelper.get().getUserDao().getById(userResponse.getUserResponse().getId());
                });
    }

}
