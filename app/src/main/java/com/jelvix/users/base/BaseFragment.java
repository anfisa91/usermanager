/*
 * Copyright (c) Anfisa Tkachova anfiska.uvarova@gmail.com
 */

package com.jelvix.users.base;

import android.app.Fragment;
import android.widget.Toast;

import com.jelvix.users.utils.FragmentsStack;

/**
 * Created by Anfisa on 18.08.2017.
 */

public class BaseFragment extends Fragment {

    protected FragmentsStack getFragmentStack() {
        return ((BaseActivity) getActivity()).getFragmentsStack();
    }

    public void setToolbarVisibility(boolean isVisible) {
        ((BaseActivity) getActivity()).setToolbarVisibility(isVisible);
    }

    public void setToolbarTitle(String title) {
        ((BaseActivity) getActivity()).setToolbarTitle(title);
    }

    public void setProgressVisibility(boolean isVisible) {
        ((BaseActivity) getActivity()).setProgressVisibility(isVisible);
    }

    public void showToast(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

}
