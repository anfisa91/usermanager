/*
 * Copyright (c) Anfisa Tkachova anfiska.uvarova@gmail.com
 */

package com.jelvix.users.base;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.jelvix.users.utils.FragmentsStack;

/**
 * Created by Anfisa on 18.08.2017.
 */

public abstract class BaseActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Set portrait orientation to activity
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    public abstract FragmentsStack getFragmentsStack();

    public abstract void setToolbarVisibility(boolean isVisible);

    public abstract void setToolbarTitle(String title);

    public abstract void setProgressVisibility(boolean isVisible);
}

