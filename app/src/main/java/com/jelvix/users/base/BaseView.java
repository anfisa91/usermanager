/*
 * Copyright (c) Anfisa Tkachova anfiska.uvarova@gmail.com
 */

package com.jelvix.users.base;

/**
 * Created by Anfisa on 19.08.2017.
 */

public interface BaseView {

    void showProgress();

    void hideProgress();

    void showRequestError(String error);
}
