package com.jelvix.users.application;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import com.jelvix.users.utils.UserPreferences;

import io.fabric.sdk.android.Fabric;

/**
 * Created by Anfisa on 16.08.2017.
 */

public class UsersApp extends Application {
    public static UsersApp instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        Fabric.with(this, new Crashlytics());
        UserPreferences.initializeInstance(instance);
    }
}
