/*
 * Copyright (c) Anfisa Tkachova anfiska.uvarova@gmail.com
 */

package com.jelvix.users.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Anfisa on 17.08.2017.
 */

public class UserPreferences {

    private static final String PREF_NAME = "user_preferences";
    private static final String USER_TOKEN = "user_token";
    private static final String CURRENT_USER_ID = "current_user_id";

    private SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor mEditor;
    private static UserPreferences sInstance;


    @SuppressLint("CommitPrefEdits")
    private UserPreferences(Context context) {
        mSharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        mEditor = mSharedPreferences.edit();
    }

    public static synchronized void initializeInstance(Context context) {
        if (sInstance == null) {
            sInstance = new UserPreferences(context);
        }
    }

    public static synchronized UserPreferences getInstance() {
        if (sInstance == null) {
            throw new IllegalStateException(UserPreferences.class.getSimpleName() +
                    " is not initialized, call initializeInstance(..) method first.");
        }
        return sInstance;
    }

    public String getUserToken() {
        return mSharedPreferences.getString(USER_TOKEN, "");
    }

    public void setUserToken(String token) {
        mEditor.putString(USER_TOKEN, token);
        mEditor.commit();
    }

    public String getCurrentUserId() {
        return mSharedPreferences.getString(CURRENT_USER_ID, "");
    }

    public void setCurrentUserId(String id) {
        mEditor.putString(CURRENT_USER_ID, id);
        mEditor.commit();
    }

    public void clearUserRegisterInfo() {
        mEditor.clear().commit();
    }

}
