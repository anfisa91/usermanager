/*
 * Copyright (c) Anfisa Tkachova anfiska.uvarova@gmail.com
 */

package com.jelvix.users.utils;

/**
 * Created by Anfisa on 18.08.2017.
 */

public class Constants {

    public static final String SERVER_URL = "https://reqres.in";

    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";
    public static final String CONFIRM_PASSWORD = "confirmPassword";
    public static final String FIRST_NAME = "first_name";
    public static final String LAST_NAME = "last_name";
    public static final String JOB = "job";

    public static final int AUTH_TYPE_LOGIN = 1;
    public static final int AUTH_TYPE_REGISTER = 2;
}
