/*
 * Copyright (c) Anfisa Tkachova anfiska.uvarova@gmail.com
 */

package com.jelvix.users.utils;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import java.util.Collection;
import java.util.regex.Pattern;

/**
 * Created by Anfisa on 18.08.2017.
 */

public class CommonUtils {

    //Hide device keyboard
    public static void hideKeyboard(Activity activity) {
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public static boolean isValidEmail(String email) {
        Pattern pattern = Pattern.compile("[a-zA-Z0-9\\'\\+\\-\\_\\%\\.]{2,256}" +
                "\\@" + "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" + "(" +
                "\\." +
                "[a-zA-Z0-9][a-zA-Z0-9\\-]{1,25}" +
                ")+");
        return pattern.matcher(email).matches();
    }

    //Check list items count, if list == null -> return 0
    public static boolean isListEmpty(Collection collection) {
        return (null == collection || collection.isEmpty() || 0 == collection.size());
    }

    public static boolean isValidName(String name) {
        Pattern pattern = Pattern.compile("[\\p{L}]{1,256}" + "['-]{0,256}" + "[\\p{L}]{1,256}");
        return pattern.matcher(name).matches();
    }
}
