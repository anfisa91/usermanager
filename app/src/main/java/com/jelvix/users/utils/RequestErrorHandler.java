/*
 * Copyright (c) Anfisa Tkachova anfiska.uvarova@gmail.com
 */

package com.jelvix.users.utils;

import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;

import org.json.JSONObject;

import okhttp3.ResponseBody;

/**
 * Created by Anfisa on 19.08.2017.
 */

public class RequestErrorHandler {

    public static String getRequesrErrorMessage(Throwable e) {
        String errorMessage = "Request error";
        if (e instanceof HttpException) {
            ResponseBody responseBody = ((HttpException) e).response().errorBody();
            errorMessage = getErrorMessage(responseBody);
        }
        return errorMessage;
    }

    private static String getErrorMessage(ResponseBody responseBody) {
        try {
            JSONObject jsonObject = new JSONObject(responseBody.string());
            return jsonObject.getString("message");
        } catch (Exception e) {
            return e.getMessage();
        }
    }
}
