/*
 * Copyright (c) Anfisa Tkachova anfiska.uvarova@gmail.com
 */

package com.jelvix.users.utils;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;

/**
 * Created by Anfisa on 18.08.2017.
 */

public class FragmentsStack {

    private FragmentManager fragmentManager;
    private int fragmentContainerID;

    public FragmentsStack(FragmentManager fragmentManager, int fragmentContainerID) {
        this.fragmentManager = fragmentManager;
        this.fragmentContainerID = fragmentContainerID;
    }

    /**
     * Set new fragment
     */
    private synchronized void set(Fragment fragment, boolean addToBackStack) {
        if (!addToBackStack) {
            clear();
            fragmentManager.beginTransaction()
                    .replace(fragmentContainerID, fragment, fragment.getClass().getSimpleName())
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).commit();
        } else {
            fragmentManager.beginTransaction()
                    .replace(fragmentContainerID, fragment, fragment.getClass().getSimpleName())
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .addToBackStack(fragment.getClass().getSimpleName()).commit();
        }
    }

    /**
     * Add new fragment
     */
    public void push(Fragment fragment) {
        set(fragment, true);
    }

    /**
     * Replace fragment
     */
    public void replace(Fragment fragment) {
        set(fragment, false);
    }

    /**
     * Get fragment name
     */
    public String peek() {
        if (getStackEntryCount() > 0) {
            return fragmentManager.getBackStackEntryAt(getStackEntryCount() - 1).getName();
        } else {
            Fragment fragment = fragmentManager.findFragmentById(fragmentContainerID);
            if (fragment != null) {
                return fragment.getTag();
            } else {
                return "";
            }
        }
    }

    public synchronized void pop() {
        fragmentManager.popBackStackImmediate();
    }

    /**
     * Clear all fragments except [Main] fragment
     */
    private synchronized boolean clear() {
        return fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    public int getStackEntryCount() {
        return fragmentManager.getBackStackEntryCount();
    }

    public FragmentManager getFragmentManager() {
        return fragmentManager;
    }
}
