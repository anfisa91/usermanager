/*
 * Copyright (c) Anfisa Tkachova anfiska.uvarova@gmail.com
 */

package com.jelvix.users.network;

import com.jelvix.users.network.entities.CreateUserRequest;
import com.jelvix.users.network.entities.CreateUserResponse;
import com.jelvix.users.network.entities.GetSingleUserReasponse;
import com.jelvix.users.network.entities.LoginRegisterRequest;
import com.jelvix.users.network.entities.LoginRegisterResponse;
import com.jelvix.users.network.entities.UsersListResponse;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Anfisa on 17.08.2017.
 */

public interface Api {

    @POST("/api/login")
    Observable<LoginRegisterResponse> loginUser(@Body LoginRegisterRequest loginRegisterRequest);

    @POST("/api/register")
    Observable<LoginRegisterResponse> registerUser(@Body LoginRegisterRequest loginRegisterRequest);

    @POST("/api/users")
    Observable<CreateUserResponse> createUser(@Body CreateUserRequest createUserRequest);

    @GET("/api/users")
    Observable<UsersListResponse> getUsersList(@Query("page") String page);

    @GET("/api/users/{id}")
    Observable<GetSingleUserReasponse> getCurrentUser(@Path("id") int id);

}
