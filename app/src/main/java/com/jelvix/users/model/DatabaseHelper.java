/*
 * Copyright (c) Anfisa Tkachova anfiska.uvarova@gmail.com
 */

package com.jelvix.users.model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.jelvix.users.application.UsersApp;
import com.jelvix.users.model.dao.UserDao;
import com.jelvix.users.model.entities.User;

import java.sql.SQLException;

/**
 * Created by Anfisa on 18.08.2017.
 */

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {
    private static final String TAG = DatabaseHelper.class.getSimpleName();
    private static final int DATABASE_VERSION = 1;
    private static DatabaseHelper databaseHelper;
    private static UserDao userDao = null;

    //Array of project database models.
    private static final Class[] DB_ENTITIES = {
            User.class
    };

    //Created database file with app package name.
    private DatabaseHelper(Context context) {
        super(context, context.getApplicationContext().getPackageName() + ".db", null, DATABASE_VERSION);
    }

    public static DatabaseHelper get() {
        if (databaseHelper == null) {
            databaseHelper = new DatabaseHelper(UsersApp.instance);
        }
        return databaseHelper;
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        for (Class entity : DB_ENTITIES) {
            try {
                TableUtils.createTable(connectionSource, entity);
            } catch (SQLException e) {
                Log.e(TAG, "error creating DB " + getDatabaseName());
                throw new RuntimeException(e);
            }
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        for (Class entity : DB_ENTITIES) {
            try {
                TableUtils.dropTable(connectionSource, entity, true);
            } catch (SQLException e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        }
        onCreate(database, connectionSource);
    }

    @Override
    public void close() {
        super.close();
    }

    public UserDao getUserDao() {
        if (userDao == null) {
            try {
                userDao = new UserDao(getConnectionSource(), User.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return userDao;
    }

    public void clearDb() {
        try {
            for (Class entity : DB_ENTITIES) {
                TableUtils.clearTable(connectionSource, entity);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public void releaseHelper() {
        OpenHelperManager.releaseHelper();
        databaseHelper = null;
    }
}