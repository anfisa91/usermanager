package com.jelvix.users.model.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import com.jelvix.users.model.entities.User;
import com.jelvix.users.network.entities.UserResponse;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Anfisa on 16.08.2017.
 */

public class UserDao extends BaseDaoImpl<User, Integer> {
    public UserDao(ConnectionSource connectionSource, Class<User> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public void save(UserResponse userResponse) {
        try {
            User user = userFromUserResponse(userResponse);
            createOrUpdate(user);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void saveAll(List<UserResponse> users) {
        try {
            callBatchTasks(() -> {
                for (UserResponse user : users) {
                    save(user);
                }
                return null;
            });
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<User> getAll() {
        try {
            return queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    public User getById(int userId) {
        try {
            return queryForId(userId);
        } catch (SQLException e) {
            e.printStackTrace();
            return new User();
        }
    }

    private User userFromUserResponse(UserResponse userResponse) {
        User user = new User();
        if (userResponse != null) {
            user.setId(userResponse.getId());
            user.setFirstName(userResponse.getFirstName());
            user.setLastName(userResponse.getLastName());
            user.setImage(userResponse.getAvatar());
        }
        return user;
    }
}
