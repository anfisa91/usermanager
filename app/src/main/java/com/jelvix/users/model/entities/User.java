package com.jelvix.users.model.entities;

import android.provider.BaseColumns;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by Anfisa on 16.08.2017.
 */
@DatabaseTable(tableName = User.TABLE_NAME)
public class User {
    static final String TABLE_NAME = "users";
    private static final String USER_ID_COLUMN = BaseColumns._ID;
    private static final String USER_FIRST_NAME_COLUMN = "userFirstName";
    private static final String USER_LAST_NAME_COLUMN = "userLastName";
    private static final String USER_AVATAR_COLUMN = "userAvatar";

    @DatabaseField(id = true, columnName = USER_ID_COLUMN)
    private int id;

    @DatabaseField(columnName = USER_FIRST_NAME_COLUMN)
    private String firstName;

    @DatabaseField(columnName = USER_LAST_NAME_COLUMN)
    private String lastName;

    @DatabaseField(columnName = USER_AVATAR_COLUMN)
    private String image;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
